namespace Incapsulation.DataAndActions
{
	public class OrderDetail
	{
		public int OrderId { get; set; }
		public int ProductId { get; set; }
		public int Count { get; set; }
		public decimal Price { get; set; }
	}
}