﻿using System;
using System.Collections.Generic;
using Incapsulation.DataAndActions.Enums;

namespace Incapsulation.DataAndActions
{
	public class Order
	{
		public int Id { get; set; }
		public DateTime OrderDate { get; set; }
		public int UserId { get; set; }
		public OrderStatus Status { get; set; }
		public List<OrderDetail> OrderDetails { get; set; }
	}
}
