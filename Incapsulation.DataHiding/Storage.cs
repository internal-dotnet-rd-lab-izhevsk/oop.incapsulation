﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Incapsulation.DataHiding
{
	public class Storage<T> where T : class
	{
		public int DataLength = 10000;

		public Storage()
		{
			FilePath = Path.GetTempFileName();
		}

		public T CurrentObject { get; set; }
		public string FilePath { get; set; }

		public int Add(T data)
		{
			CurrentObject = data;
			using var stream = OpenFile();

			BinaryFormatter bf = new BinaryFormatter();
			using MemoryStream ms = new MemoryStream();

			bf.Serialize(ms, data);
			stream.Seek(0, SeekOrigin.End);
			stream.Write(ms.ToArray());
			stream.Write(new byte[DataLength - ms.Length]);
			var key = stream.Position;

			stream.Flush();
			stream.Close();

			return (int)key;
		}

		public Stream OpenFile()
		{
			return File.Open(FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
		}
	}
}